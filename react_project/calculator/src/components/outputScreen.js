import React from 'react';
import OutputScreenRow from './outputScreenRow.js';

export default function OutputScreen(props) {
  return (
    <div className="screen">
      <OutputScreenRow value={props.question} />
      <OutputScreenRow value={props.answer} />
    </div>
  )
};